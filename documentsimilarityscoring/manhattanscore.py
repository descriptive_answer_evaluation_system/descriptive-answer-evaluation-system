from nltk import sent_tokenize,word_tokenize,PorterStemmer
from nltk.corpus import wordnet,stopwords
from nltk.tokenize import PunktSentenceTokenizer
from nltk.stem import WordNetLemmatizer
from nltk.tag import pos_tag
import math,re
from collections import Counter
import numpy as np


input_text=open("model.txt","r")
input_text1=open("ans1.txt","r")

text=input_text.read()
text1=input_text1.read()

input_text.close()
input_text1.close()

sentences=sent_tokenize(text)
sentences1=sent_tokenize(text1)

N=len(sentences)
N1=len(sentences1)

N6= N1


ps=PorterStemmer()
lemmatizer=WordNetLemmatizer()
stop_words=stopwords.words('english')
special=['.',',','\'','"','-','/','*','+','=','!','@','$','%','^','&','``','\'\'','We','The','This']






def normalise(word):
    word = word.lower()
    word = ps.stem(word)
    return word



def manhattan_distance(x,y):
	return sum([abs(x[a]-y[b]) for a in x for b in y if a==b])
     

def text_to_vector(text):
     words = word_tokenize(text)
     vec=[]
     for word in words:
         if(word not in stop_words):
             if(word not in special):
                 w=normalise(word);
                 vec.append(w);
     #print Counter(vec)
     return Counter(vec)



def docu_to_vector(sent):
     vec=[]
     for text in sent:
         words = word_tokenize(text)
         for word in words:
             if(word not in stop_words):
                 if(word not in special):
                     w=normalise(word);
                     vec.append(w);
     #print Counter(vec)
     return Counter(vec)




def f_s_to_s(sent):
    cosine_mat=np.zeros(N+1)
   
    row=0
    for text in sentences:
        maxi=0
        vector1 = text_to_vector(text)
        for text1 in sent:
            vector2 = text_to_vector(text1)
            cosine = manhattan_distance(vector1, vector2)

        



           
            if(maxi<cosine):
                maxi=cosine
               
        cosine_mat[row]=maxi
             
        row+=1
        
    return cosine_mat   
    
def print_score():
    
    sent = sentences1 + sentences2 + sentences3 + sentences4 + sentences5
    output_text=open("res.txt","w")
    output_text.write("\n Mark obtained is  \n\n")
    for i in range(sent):
      output_text.write(print_score[i])
      output_text.close()
    print("\n Successfully evaluated")
def main():
    
    sent = sentences1 
    max_mark=100
    mat = f_s_to_s(sentences1)
   
          
    print_score

    cnt = docu_to_vector(sent)
    cnt = cnt.most_common(10)
        
    thematic=[]
    for i in range(10):
        thematic.append(cnt[i][0])

    sum1=0

    thematic = ",".join(str(x) for x in thematic)
    thematic = text_to_vector(thematic)
    for i in sentences1:
        i = text_to_vector(i)
        sum1=sum1+ manhattan_distance(thematic,i)
        

    

    point1= sum(mat)
    #print point1
    
    score1=point1*  max_mark*1/(2*N)
    
    score1=score1+ (sum1/2)
    
    print score1
    
if __name__ == '__main__':
    main()






