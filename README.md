# Descriptive Answer Evaluation System

 The proposed Descriptive Answer Evaluation System introduces a
novel approach that combines the pattern mining unsupervised tech-
nique with the similarity measurement.

In this, the sequential word patterns that are more common (than the
rest of the pat terns)among student answers are identified along with
the count for each pattern.  Along with this, the students answers are
matched against the pre-graded answers or manually crafted key con-
cepts using the concept of similarity measurement.


 Finally a weighted score function is generated for each answer script
and evaluated accordingly